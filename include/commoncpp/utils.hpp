#ifndef UTILS_HPP
#define UTILS_HPP
#include <string>
#include <string_view>
#ifndef COMMONCPP_LITE
#include <vector>
#endif
#include <utility>


namespace common {
namespace utils {
#ifndef COMMONCPP_LITE
std::vector<std::string_view> str_split(std::string_view s, char delimiter, size_t times = -1);
#endif
std::pair<std::string_view, std::string_view> str_split_once(std::string_view s, std::string_view delim);
std::pair<std::string_view, std::string_view> str_split_once(std::string_view s, char delim);
bool str_replace_in_place(std::string& subject, std::string_view search, const std::string& replace);
std::string_view max_words(std::string_view text, unsigned count);
bool contains(std::string_view value, std::string_view other);
bool starts_with(std::string_view value, std::string_view beginning);
bool ends_with(std::string_view value, std::string_view ending);
bool force_trailing(std::string& value, std::string_view ending);
bool chop_down(std::string& s, char delim);
#ifndef COMMONCPP_LITE
std::string remove_nonprintable(std::string_view);
std::string read_text_file(const std::string& path);
#endif

[[noreturn]] inline void unreachable() {
    // Uses compiler specific extensions if possible.
    // Even if no extension is used, undefined behavior is still raised by
    // an empty function body and the noreturn attribute.
#ifdef __GNUC__ // GCC, Clang, ICC
    __builtin_unreachable();
#elifdef _MSC_VER // MSVC
    __assume(false);
#endif
}
}
}
#endif // UTILS_HPP
