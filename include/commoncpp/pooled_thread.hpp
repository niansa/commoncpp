#ifndef COMMONCPP_LITE
#ifndef SCHEDULED_THREAD_HPP
#define SCHEDULED_THREAD_HPP
#include <functional>
#include <future>
#include <mutex>
#include <queue>
#include <thread>



namespace common {
// This thread could be part of a thread pool
class PooledThread {
    using QueueEntry = std::function<void ()>;

    std::thread thread;
    std::mutex queue_mutex;
    std::queue<QueueEntry> queue;
    std::mutex conditional_mutex;
    std::condition_variable conditional_lock;
    bool shutdown_requested = false;
    bool joined = false;

    void main_loop();

public:
    PooledThread() {}

    // MUST NOT already be running
    void start() {
        thread = std::thread([this] () {
            main_loop();
        });
    }

    // Can be called from anywhere
    void enqueue(std::function<void ()>&& task_fcn) {
        // Enqueue function
        {
            std::scoped_lock L(queue_mutex);
            queue.emplace(QueueEntry{std::move(task_fcn)});
        }

        // Notify thread
        conditional_lock.notify_one();
    }

    // MUST already be running
    void wait() {
        joined = true;
        thread.join();
    }

    // MUST already be running
    void shutdown() {
        enqueue([this] () {
                    shutdown_requested = true;
                });
    }
};
}

namespace [[deprecated("Use the common namespace instead of commoncpp")]] commoncpp {
using namespace common;
}
#endif // SCHEDULED_THREAD_HPP
#endif // COMMONCPP_LITE
