#ifndef COMMONCPP_LITE
#ifndef CONFIG_HPP
#define CONFIG_HPP
#include <string>
#include <unordered_map>
#include <stdexcept>


namespace common {
class Configuration {
public:
    struct Exception : public std::runtime_error {
        using std::runtime_error::runtime_error;
    };

protected:
    using KeyValueMap = std::unordered_map<std::string, std::string>;

    bool ignore_environment = false;

    static
    bool parse_bool(const std::string& value);

    static
    std::string parse_string(const std::string& value); // Unescapes string

    static
    bool file_exists(std::string_view path);

    static
    KeyValueMap file_parser(const std::string& path);
    static
    KeyValueMap environment_parser();

    virtual void fill(KeyValueMap&&, bool ignore_extra = false) = 0;
    virtual void check() const {} // Should throw on error

public:
    Configuration() {}
    Configuration(Configuration&) = delete;
    Configuration(const Configuration&) = delete;
    Configuration(Configuration&&) = delete;
    Configuration(const Configuration&&) = delete;
    virtual ~Configuration() {}

    virtual void parse(const std::string& file = "");
};
}
#endif // CONFIG_HPP
#endif // COMMONCPP_LITE
