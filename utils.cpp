#include "utils.hpp"

#ifndef COMMONCPP_LITE
#include <sstream>
#include <fstream>
#include <stdexcept>
#include <cerrno>
#endif



namespace common {
namespace utils {
#ifndef COMMONCPP_LITE
std::vector<std::string_view> str_split(std::string_view s, char delimiter, size_t times) {
    std::vector<std::string_view> to_return;
    decltype(s.size()) start = 0, finish = 0;
    while ((finish = s.find_first_of(delimiter, start)) != std::string_view::npos) {
        to_return.emplace_back(s.substr(start, finish - start));
        start = finish + 1;
        if (to_return.size() == times) { break; }
    }
    to_return.emplace_back(s.substr(start));
    return to_return;
}
#endif

std::pair<std::string_view, std::string_view> str_split_once(std::string_view s, std::string_view delim) {
    // Find the delimiter
    auto pos = s.find(delim);
    if (pos == s.npos) {
        return {s, ""};
    }
    // Split there
    return {s.substr(0, pos), s.substr(pos+delim.size(), s.size()-1)};
}
std::pair<std::string_view, std::string_view> str_split_once(std::string_view s, char delim) {
    // Find the delimiter
    auto pos = s.find(delim);
    if (pos == s.npos) {
        return {s, ""};
    }
    // Split there
    return {s.substr(0, pos), s.substr(pos+1, s.size()-1)};
}

bool str_replace_in_place(std::string& subject, std::string_view search,
                         const std::string& replace) {
    if (search.empty()) return false;
    bool had_effect = false;
    size_t pos = 0;
    while ((pos = subject.find(search, pos)) != std::string::npos) {
         subject.replace(pos, search.length(), replace);
         pos += replace.length();
         had_effect = true;
    }
    return had_effect;
}

std::string_view max_words(std::string_view text, unsigned count) {
    unsigned word_len = 0,
             word_count = 0,
             idx;
    // Get idx after last word
    for (idx = 0; idx != text.size() && word_count != count; idx++) {
        char c = text[idx];
        if (c == ' ' || word_len == 8) {
            if (word_len != 0) {
                word_count++;
                word_len = 0;
            }
        } else {
            word_len++;
        }
    }
    // Return resulting string
    return {text.data(), idx};
}

bool contains(std::string_view value, std::string_view other) {
    return value.find(other) != value.npos;
}

bool starts_with(std::string_view value, std::string_view beginning) {
    return value.find(beginning) == 0;
}

bool ends_with(std::string_view value, std::string_view ending) {
    if (ending.size() > value.size()) return false;
    return std::equal(ending.rbegin(), ending.rend(), value.rbegin());
}

bool force_trailing(std::string &value, std::string_view ending) {
    bool fres = ends_with(value, ending);
    if (!fres) value.append(ending);
    return fres;
}

bool chop_down(std::string& s, char delim) {
    const auto pos = s.find(delim);
    if (pos == s.npos) return false;
    s.erase(pos);
    return true;
}

#ifndef COMMONCPP_LITE
std::string remove_nonprintable(std::string_view str) {
    std::string fres;
    for (const char c : str) {
        if (std::isprint(c)) fres.push_back(c);
    }
    return fres;
}

std::string read_text_file(const std::string& path) {
    std::ifstream in(path);
    if (!in) throw std::system_error(std::error_code(errno, std::system_category()), path);
    std::ostringstream sstr;
    sstr << in.rdbuf();
    return sstr.str();
}

#endif
}
}
